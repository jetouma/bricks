# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

#### Syntax

* make `0` valid number literal
* parse unary minus
* parse `k` and `m` number notation
* parse `.` and `,` as decimal separator

#### Notes

* add files and folders
* save all changes in local db
* sync changes between tabs
* toggle files in folder
* delete files and folders

### Fixed

* bug with line number expressions

### Changed

* remove operations highlight

## 0.1.0 / 2020-07-06

* initial release
