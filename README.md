# Bricks Calculations

[![test](https://img.shields.io/github/workflow/status/ttypic/bricks/Test?label=tests&style=flat-square)](https://github.com/ttypic/bricks/actions)
[![coverage-status](https://img.shields.io/codecov/c/github/ttypic/bricks.svg?style=flat-square)](https://codecov.io/gh/ttypic/bricks)
[![Netlify Status](https://api.netlify.com/api/v1/badges/6adfad16-7706-4a83-94d8-6dc1ffb390f9/deploy-status)](https://app.netlify.com/sites/epic-pasteur-63fa87/deploys)

Create and share fast calculations right in your browser
