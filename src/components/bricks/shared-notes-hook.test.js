import { useSharedNotesReader } from 'components/bricks/shared-notes-hook';
import { renderHook } from '@testing-library/react-hooks';
import { hasSharedNoteParams, clearParams, read } from 'utils/hash-utils';

jest.mock('utils/hash-utils', () => ({ clearParams: jest.fn(), hasSharedNoteParams: jest.fn(), read: jest.fn() }));

it('should not call handler if not inited', () => {
    let inited = false;
    hasSharedNoteParams.mockReturnValue(true);
    const handler = jest.fn();

    const { rerender } = renderHook(() => {
        useSharedNotesReader(inited, handler);
    });

    rerender();
    expect(handler).toBeCalledTimes(0);
});

it('should not call handler if url has no params', () => {
    let inited = false;
    hasSharedNoteParams.mockReturnValue(false);
    const handler = jest.fn();

    const { rerender } = renderHook(() => {
        useSharedNotesReader(inited, handler);
    });

    rerender();
    expect(handler).toBeCalledTimes(0);
});

it('should call handler if has no params', () => {
    let inited = true;
    hasSharedNoteParams.mockReturnValue(true);
    const readObject = {};
    read.mockReturnValue(readObject);

    const handler = jest.fn();

    const { rerender } = renderHook(() => {
        useSharedNotesReader(inited, handler);
    });

    rerender();
    expect(handler).toHaveBeenNthCalledWith(1, readObject);
    expect(clearParams).toBeCalledTimes(1);
});
