import React from 'react';
import { createSvgIcon } from 'components/svg-icons/create-svg-icon';

const ClosedFolder = createSvgIcon(
    <path d="M20 6h-8l-2-2H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm0 12H4V6h5.17l1.41 1.41.59.59H20v10z" />
);
export default ClosedFolder;
