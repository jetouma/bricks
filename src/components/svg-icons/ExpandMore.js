import React from 'react';
import { createSvgIcon } from 'components/svg-icons/create-svg-icon';

const ExpandMore = createSvgIcon(<path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" />);

export default ExpandMore;
