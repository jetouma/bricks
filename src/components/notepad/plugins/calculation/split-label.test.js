import splitLabel from 'components/notepad/plugins/calculation/split-label';

test('it removes label', () => {
    const [expression, label] = splitLabel('some label: 2 * 2');
    expect(expression).toBe(' 2 * 2');
    expect(label).toBe('some label');
});

test('it do nothing if there is no labels', () => {
    const [expression, label] = splitLabel('2 * 2');
    expect(expression).toBe('2 * 2');
    expect(label).toBe(undefined);
});
