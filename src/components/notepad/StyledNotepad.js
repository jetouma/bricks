import styled from 'styled-components/macro';
import { ellipsis } from 'polished';
import { md, sm, xs } from 'constants/device-widths';
import { COLUMN_MODE } from 'components/settings-context/view-modes';

const getBasedOnViewModeStyles = ({ viewMode }) => {
    if (viewMode === COLUMN_MODE) {
        return `
            position: absolute;
            right: -9rem;
            width: 6rem;
            margin: -30px 0 0 0;

            /* stylelint-disable*/
            @media (max-width: ${xs}px) {
                & {
                    right: -7.2rem;
                    width: 4rem;
                }
            }
            /* stylelint-enable*/
        `;
    } else {
        return `
            margin: 0 0 0.5rem 0;

            background: var(--editor-badge-bg-color);
        `;
    }
};

const StyledNotepad = styled.main`
    position: relative;

    box-sizing: border-box;

    width: ${({ viewMode }) => (viewMode === COLUMN_MODE ? '600px' : '100%')};
    margin: 0.5rem 0.25rem 0.5rem 0;
    overflow: visible;

    color: var(--editor-main-color);

    background: var(--editor-bg-color);

    .ProseMirror {
        position: relative;

        padding: 0 1rem;

        font-size: 0.875rem;

        line-height: 1.5;
        white-space: pre-wrap;
        word-wrap: break-word;
        overflow-wrap: break-word;
        hyphens: auto;

        outline: none;
        font-variant-ligatures: none;
    }

    .ProseMirror p {
        margin: 0 0 0.5rem 0;

        color: var(--editor-main-color);
    }

    .value-badge {
        display: block;
        padding: 0 1rem;
        overflow: hidden;

        border-radius: 0.25rem;

        color: var(--editor-badge-color);
        white-space: nowrap;
        text-align: left;
        text-overflow: ellipsis;

        cursor: pointer;

        user-select: none;

        ${getBasedOnViewModeStyles}
    }

    .line-number {
        position: absolute;
        left: -22px;

        width: 18px;
        padding-top: 2px;

        color: var(--editor-line-number-color);

        font-size: var(--editor-line-number-font-size);
        text-align: right;

        user-select: none;

        ${ellipsis(18)}
    }

    .expression {
        display: none;
    }

    .expression-badge {
        display: inline;
        box-sizing: border-box;
        margin: 0 2px;

        overflow: hidden;

        border: 1px solid var(--editor-exp-border-color);
        border-radius: 2px;

        color: var(--editor-number-color);

        background-color: #f2f2f2;
    }

    .expression-badge-line {
        display: inline;

        color: var(--editor-line-number-color);
        font-size: 10px;
    }

    .reused-badge-highlighter {
        border-radius: 0.25rem;

        background: #dbe9ff;
    }

    .numbers-token {
        color: var(--editor-number-color);
    }

    .operations-token {
        color: rgba(0, 114, 130, 0.8);
    }

    .labels-token {
        color: rgba(0, 0, 0, 0.6);
        font-weight: 600;
    }

    @media (max-width: ${md}px) {
        & {
            width: calc(100% - 6px);

            border-radius: 0;
        }
    }

    @media (max-width: ${sm}px) {
        & {
            width: calc(100% - 6px);

            border-radius: 0;
        }

        .ProseMirror {
            font-size: 1rem;
        }
    }
`;

export default StyledNotepad;
