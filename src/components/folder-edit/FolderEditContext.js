import { createContext } from 'react';

const FolderEditContext = createContext({
    editedFolderId: null,
    selectFolder: () => {},
    commitChanges: () => {}
});

export default FolderEditContext;
