import React from 'react';
import { render } from '@testing-library/react';
import App from 'components/app/App';

jest.mock('components/files-context/FilesProvider', () => ({ children }) => children);
jest.mock('components/settings-context/SettingsProvider', () => ({ children }) => children);
jest.mock('components/notepad/StatefulNotepad', () => () => null);

test('it renders without errors', () => {
    render(<App />);
});
