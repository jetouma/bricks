import validateKeywords from './percent-token-utils';

function withProp(tokens) {
    return tokens.map(tok => {
        return { image: tok };
    });
}

test('proper percent expression', () => {
    let tokens = withProp(['20', 'is', '10%', 'of']);
    expect(validateKeywords('20 is 10% of', tokens)).toBe(tokens);
});

test('proper percent expression 5 is 16%     of', () => {
    let tokens = withProp(['5', 'is', '16%', 'of']);
    expect(validateKeywords('5   is 16%   of', tokens)).toBe(tokens);
});

test("exclude 'is': is it true?", () => {
    let tokens = withProp(['is']);
    let expected = withProp([]);
    expect(validateKeywords('is it true?', tokens)).toStrictEqual(expected);
});

test("exclude 'is': Yes it is", () => {
    let tokens = withProp(['is']);
    let expected = withProp([]);
    expect(validateKeywords('Yes it is', tokens)).toStrictEqual(expected);
});

test("exclude 'is': Yes 5 + 5 it is", () => {
    let tokens = withProp(['5', '+', '5', 'is']);
    let expected = withProp(['5', '+', '5']);
    expect(validateKeywords('Yes 5 + 5 it is', tokens)).toStrictEqual(expected);
});

test("exclude 'is': This is 23 * 2 as example", () => {
    let tokens = withProp(['is', '23', '*', '2']);
    let expected = withProp(['23', '*', '2']);
    expect(validateKeywords('This is 23 * 2 as example', tokens)).toStrictEqual(expected);
});

test("exclude 'is': 20 is more then 10", () => {
    let tokens = withProp(['20', 'is', '10%']);
    let expected = withProp(['20', '10%']);
    expect(validateKeywords('20 is 10', tokens)).toStrictEqual(expected);
});

test("exclude 'of': as of this ", () => {
    let tokens = withProp(['of']);
    let expected = withProp([]);
    expect(validateKeywords('as of this ', tokens)).toStrictEqual(expected);
});

test("exclude 'to': as this to that", () => {
    let tokens = withProp(['to']);
    let expected = withProp([]);
    expect(validateKeywords('as this to that ', tokens)).toStrictEqual(expected);
});

test("exclude 'of': as of this 23 - 2 example", () => {
    let tokens = withProp(['of', '23', '-', '2']);
    let expected = withProp(['23', '-', '2']);
    expect(validateKeywords('as of this 23 - 2 example', tokens)).toStrictEqual(expected);
});

test("exclude 'of': of 5*5", () => {
    let tokens = withProp(['of', '5', '*', '5']);
    let expected = withProp(['5', '*', '5']);
    expect(validateKeywords('of 5 * 5', tokens)).toStrictEqual(expected);
});

test("exclude '%', 'of': What % of this I will have", () => {
    let tokens = withProp(['%', 'of']);
    let expected = withProp([]);
    expect(validateKeywords('What % of this I will have', tokens)).toStrictEqual(expected);
});
