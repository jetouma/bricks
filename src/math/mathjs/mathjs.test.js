import { getMath } from 'math/mathjs/mathjs';

const math = getMath();

test('bin(22)', () => {
    expect(math.eval('bin(22)', {})).toEqual({ isBinary: true, result: '0b10110' });
});
test('oct(22)', () => {
    expect(math.eval('oct(22)', {})).toEqual({ isBinary: true, result: '0o26' });
});

test('hex(22)', () => {
    expect(math.eval('hex(22)', {})).toEqual({ isBinary: true, result: '0x16' });
});

test('number(0x16 + 0x16)', () => {
    expect(math.eval('number(0x16 + 0x16)', {})).toEqual({ isBinary: true, result: 44 });
});

test('sqrt(16)', () => {
    expect(math.eval('sqrt(16)', {})).toEqual({ isBinary: true, result: 4 });
});
