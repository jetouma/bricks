import { tryEvaluateUsingMathJs } from 'math/interpreter';

describe('arithmetics tests', () => {
    describe('grammar tests', () => {
        test('redundant + parser errors', () => {
            const { result } = tryEvaluateUsingMathJs('2 + + + 1 + 1');
            expect(result.value).toBe(4);
        });
        test('unexpected symbols error', () => {
            const { result, lexerErrors } = tryEvaluateUsingMathJs('2 + @@1 + 1');
            expect(lexerErrors.length).toBe(1);
            expect(result.value).toBe(4);
        });
        test('Valid space in number', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1 000 + 5');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 1005);
        });

        test('Ignore redundant spaces', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('2-    1   + 1');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 2);
        });

        test('Ignore non-grammar symbols', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('From 4 extract -2');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 2);
        });

        test('Ignore sole commented number', () => {
            const { parserErrors, lexerErrors } = tryEvaluateUsingMathJs('\\1000');
            expectParseError(lexerErrors, parserErrors);
        });

        test('Ignore commented number but respect non-commented', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('\\12  22');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 22);
        });

        test('Ignore commented number and other symbols but respect expressions', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('comment \\1000 This 10 and +10');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 20);
        });

        test('Ignore all commented numbers but respect expressions', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs(
                'comment \\1000 This \\2 10 and +10  \\20 \\30'
            );
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 20);
        });

        test('Ignore incorrect thousand-K shortcut', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('43Kg + 2');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 45);
        });

        test('Ignore incorrect thousand-K shortcut without space (left operand)', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('43K+ 2');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 43002);
        });

        test('Respect correct thousand-K shortcut (right operand)', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('2 + 43K');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 43002);
        });

        test('Respect correct thousand-K shortcut (left operand)', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('43K + 2');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 43002);
        });

        test('Ignore incorrect million-M shortcut', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('43Ml + 2');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 45);
        });

        test('Respect correct million-M shortcut', () => {
            const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('43M + 2');
            expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 43000002);
        });
    });

    test('eval 2 + 2', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('2 + 2');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 4);
    });

    test('eval 2 + 2 * 3', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('2 + 2 * 3');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 8);
    });

    test('eval 1 * 2 + 3 * 4', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1 * 2 + 3 * 4');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 14);
    });

    test('eval (1 + 1) / 2', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('(1 + 1) / 2');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 1);
    });

    test('eval 2 ^ 3 ^ 2', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('2 ^ 3 ^ 2');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 512);
    });

    test('eval (2 ^ 3) ^ 2', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('(2 ^ 3) ^ 2');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 64);
    });

    test('2^3', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('2^3');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 8);
    });

    test('eval -1', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('-1');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, -1);
    });

    test('eval -1 + 5', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('-1 + 5');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 4);
    });

    test('eval 10 - 1000', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10 - 1000');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, -990);
    });

    test('eval 10 - 1000 * -1', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10 - 1000 * -1');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 1010);
    });

    test('eval 5 * -1', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('5 * -1');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, -5);
    });

    test('eval 10% * 1000', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10% * 1000');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 100);
    });

    test('eval 100 * 0', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('100 * 0');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 0);
    });

    test('eval 0 * 100', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('0 * 100');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 0);
    });

    test('eval 100 / 0', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('100 / 0');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, Infinity);
    });

    test('eval 0 / 100', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('0 / 100');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 0);
    });

    test('eval 100 * 0.1', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('100 * 0.1');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 10);
    });
});

describe('percent tests', () => {
    test('literal 10%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 10, 'percent');
    });

    test('eval 1000 + 10%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1000 + 10%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 1100);
    });

    test('eval 2 + 10%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('2 + 10%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 2.2);
    });

    test('eval 1000 - 10%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1000 - 10%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 900);
    });

    test('eval 1000 - 100%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1000 - 100%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 0);
    });

    test('eval 1000 - 110%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1000 - 110%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, -100);
    });

    test('eval 1000 - 10% - 10 %', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1000 - 10% - 10 %');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 810);
    });

    test('eval 1000 - (10% - 10%)', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1000 - (10% - 10%)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 1000);
    });

    test('eval 10% * 10%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10% * 10%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 0.01);
    });

    test('eval 102 * 10%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('102 * 10%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 10.2);
    });

    test('eval 1000 * 10%', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('1000 * 10%');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 100);
    });

    test('eval 10% * 1010', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10% * 1010');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 101);
    });

    test('eval 20 is 10% of', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('20 is 10% of');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 200);
    });

    /**
     * todo need to fix this bug! somehow fraction percent affects parser
     */
    test.skip('eval 160 is 8.5% of', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('160 is 8.5% of');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 18823.529411765);
    });

    test('eval 450 is 15% of some other irrelevant text', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs(
            '450 is 15% of some other irrelevant text'
        );
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 3000);
    });

    test('eval 450 isSSS 15% of some other irrelevant text', () => {
        const { parserErrors, lexerErrors } = tryEvaluateUsingMathJs('450 isSSS 15% of some other irrelevant text');
        expectParseError(lexerErrors, parserErrors);
    });

    test('eval 10% of 1010', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10% of 1010');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 101);
    });

    test('eval 8.5% of 2000', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('8.5% of 2000');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 170);
    });

    test('eval 8.5% of 200.5', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('8.5% of 200.5');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 17.0425);
    });

    test('eval 10% of (10 + 1000)', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10% of (10 + 1000)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 101);
    });
    test('eval 32 of 556 to %', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('32 of 556 to %');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 5.7554, 'percent');
    });

    test('eval 25% ofw 2010', () => {
        const { parserErrors, lexerErrors } = tryEvaluateUsingMathJs('25% ofw 2010');
        expectParseError(lexerErrors, parserErrors);
    });

    test('eval 10 of 1000 to %', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('10 of 1000 to %');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 1, 'percent');
    });

    test('eval 25 of 1000 to %', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('25 of 1000 to %');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 2.5, 'percent');
    });

    test("ignore invalid usage of 'is' literal", () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('this is 204');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 204);
    });

    test("ignore invalid usage of 'to' literal", () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('Calculate to 204');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 204);
    });

    test("ignore invalid usage of 'to, is, of, %' literal", () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('Calculate is this to % 204 of');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 204);
    });
});
describe('variables tests', () => {
    test('math-js variable', () => {
        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs('my_variable = 42');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 42);

        //continue...
    });

    test('eval with passing variable scopes', () => {
        let { result: r1 } = tryEvaluateUsingMathJs('initial_speed = 3');
        let { result: r2 } = tryEvaluateUsingMathJs('acceleration = 5', r1.scope);
        let { result: r3 } = tryEvaluateUsingMathJs('time = 2', r2.scope);

        const { result, parserErrors, lexerErrors } = tryEvaluateUsingMathJs(
            'distance = initial_speed * time + 0.5 * acceleration * time ^ 2',
            r3.scope
        );
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 16);
    });
});

describe('unit conversion tests', () => {
    test.skip('1 kg to grams', () => {
        //{"fixPrefix": true, "mathjs": "Unit", "unit": "grams", "value": {"mathjs": "BigNumber", "value": "1000"}}
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('1 kg to grams');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 1000, 'unit');
    });
});

describe('logic tests', () => {
    test('42 == 42', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('42 == 42');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, true, 'bool');
    });
    test('true or false', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('true or false');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, true, 'bool');
    });
    test('not true', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('not true');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, false, 'bool');
    });
});

describe('binary tests', () => {
    test('hex(42)', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('hex(42)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, '0x2a', 'binary');
    });
    test('bin(42)', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('bin(42)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, '0b101010', 'binary');
    });
    test('number(42)', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('dec(42)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 42, 'number');
    });
    test('oct(42)', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('oct(42)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, '0o52', 'binary');
    });
    //todo this must return number type!
    test('number(0o52)', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('number(0o52)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 42, 'binary');
    });
    test('number(0b101010)', () => {
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('number(0b101010)');
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 42, 'binary');
    });
});

describe('rounding tests', () => {
    test('round 2 decimals', () => {
        let settings = [{ name: 'round', value: 2 }];
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('10 / 3', {}, settings);
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 3.33, 'number');
    });
    test('round 2 decimals in text', () => {
        let settings = [{ name: 'round', value: 2 }];
        const { lexerErrors, parserErrors, result } = tryEvaluateUsingMathJs('some text 10 / 3', {}, settings);
        expectNoErrorsAndResultToBe(lexerErrors, parserErrors, result, 3.33, 'number');
    });
});

function expectParseError(lexerErrors, parserErrors) {
    expect(lexerErrors.length).toBe(0);
    expect(parserErrors.length).toBe(1);
}

function expectNoErrorsAndResultToBe(lexerErrors, parserErrors, actual, expectedValue, expectedType = 'number') {
    expect(lexerErrors.length).toBe(0);
    expect(parserErrors.length).toBe(0);
    expect(actual.value).toBe(expectedValue);
    expect(actual.type).toBe(expectedType);
}
