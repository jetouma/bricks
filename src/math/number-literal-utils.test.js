import { parseNumber } from 'math/number-literal-utils';
import { getMath } from 'math/mathjs/mathjs';

const math = getMath().getMathJs();

test('it parse 0', () => {
    expect(parseNumber(math, '0')).toBe(0);
});

test('it parse K-notation', () => {
    expect(parseNumber(math, '1K')).toBe(1000);
    expect(parseNumber(math, '1k')).toBe(1000);
    expect(parseNumber(math, '2K')).toBe(2000);
    expect(parseNumber(math, '2k')).toBe(2000);
    expect(parseNumber(math, '1kg')).toBe(1);
    expect(parseNumber(math, '22kg')).toBe(22);
    expect(parseNumber(math, '33Kg')).toBe(33);
    expect(parseNumber(math, '41K,')).toBe(41);
});

test('it parse M-notation', () => {
    expect(parseNumber(math, '1M')).toBe(1000000);
    expect(parseNumber(math, '1m')).toBe(1000000);
    expect(parseNumber(math, '2mg')).toBe(2);
    expect(parseNumber(math, '1mg')).toBe(1);
    expect(parseNumber(math, '22mg')).toBe(22);
    expect(parseNumber(math, '33Mg')).toBe(33);
    expect(parseNumber(math, '41M,')).toBe(41);
});

test('it parse float value with `.` and `,`', () => {
    expect(parseNumber(math, '0.1')).toBe(0.1);
    expect(parseNumber(math, '0,1')).toBe(0.1);
});
