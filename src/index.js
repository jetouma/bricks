import React from 'react';
import ReactDOM from 'react-dom';
import App from 'components/app/App';
import { createSwConfig } from 'utils/create-sw-config';
import * as serviceWorker from './serviceWorker';
import SnackContainer from 'components/snack-container/SnackContainer';

ReactDOM.render(
    <React.StrictMode>
        <App />
        <SnackContainer />
    </React.StrictMode>,
    document.getElementById('root')
);

serviceWorker.register(createSwConfig());
