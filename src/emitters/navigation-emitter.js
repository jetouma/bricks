import createEventEmitter from 'utils/create-event-emitter';
import * as visibilityTypes from 'components/navigation/navigation-visibility-types';

const navigationEmitter = createEventEmitter();

export const subscribe = navigationEmitter.subscribe;

export const show = () => navigationEmitter.emit(visibilityTypes.VISIBLE);
