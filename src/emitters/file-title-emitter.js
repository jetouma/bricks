import createEventEmitter from 'utils/create-event-emitter';

const fileTitleEmitter = createEventEmitter();

export const subscribe = fileTitleEmitter.subscribe;

export const emitChange = ({ uuid, name }) => fileTitleEmitter.emit({ uuid, name });
