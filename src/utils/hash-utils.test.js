import { buildShareUrl, clearParams, hasSharedNoteParams, read } from 'utils/hash-utils';

describe('buildShareUrl common tests', () => {
    it('escapes spaces in title', () => {
        delete window.location;
        window.location = new URL('http://bricks.app');
        const url = buildShareUrl({ title: '1 2 3', doc: {} });
        expect(url).toBe('http://bricks.app/?t=1+2+3&d=N4XyA');
    });
});

describe('read form url common tests', () => {
    it('reads escaped title right', () => {
        delete window.location;
        window.location = new URL('http://bricks.app?t=1+2+3&d=N4XyA');
        const { doc, title } = read();
        expect(doc).toEqual({});
        expect(title).toBe('1 2 3');
    });

    it('returns empty object if one of parameters is not set', () => {
        delete window.location;
        window.location = new URL('http://bricks.app?t=1+2+3');
        expect(read()).toEqual({});
    });

    it('returns empty object if can not parse doc', () => {
        delete window.location;
        window.location = new URL('http://bricks.app?t=1+2+3&d=N4');
        expect(read()).toEqual({});
    });
});

describe('hasSharedNoteParams common tests', () => {
    it('returns true if title and doc parameters are set', () => {
        expect(hasSharedNoteParams(new URLSearchParams('t=1+2+3&d=N4'))).toBe(true);
    });

    it('returns false if only title is set', () => {
        expect(hasSharedNoteParams(new URLSearchParams('d=N4'))).toBe(false);
    });

    it('returns false if only title is set', () => {
        expect(hasSharedNoteParams(new URLSearchParams('t=1+2+3'))).toBe(false);
    });

    it('uses location search params by default', () => {
        delete window.location;
        window.location = new URL('http://bricks.app?t=1+2+3&d=N4');
        expect(hasSharedNoteParams()).toBe(true);
    });
});

describe('clearParams common tests', () => {
    it('clears params', () => {
        delete window.location;
        window.location = new URL('http://bricks.app?t=1+2+3&d=N4');
        jest.spyOn(window.history, 'replaceState');
        window.history.replaceState.mockImplementation((state, title, url) => {
            window.location = new URL(url);
        });
        clearParams();
        expect(window.location.toString()).toBe('http://bricks.app/');
    });
});
