const baseEncode = (state, { keyRemap, valueRemap }) => {
    if (typeof state !== 'object') return state;

    if (Array.isArray(state)) return state.map(value => baseEncode(value, { keyRemap, valueRemap }));

    const encoded = {};
    Object.keys(state).forEach(key => {
        if (key in valueRemap && state[key] in valueRemap[key]) {
            encoded[keyRemap[key]] = valueRemap[key][state[key]];
        } else if (key in keyRemap) {
            encoded[keyRemap[key]] = baseEncode(state[key], { keyRemap, valueRemap });
        } else {
            encoded[key] = baseEncode(state[key], { keyRemap, valueRemap });
        }
    });

    return encoded;
};

export const encode = state => {
    const keyRemap = {
        content: '_c',
        type: '_t',
        attrs: '_a',
        dataId: '_d',
        text: '_x'
    };

    const valueRemap = {
        type: { paragraph: 'p', text: 'x' }
    };

    return baseEncode(state, { keyRemap, valueRemap });
};

export const decode = encoded => {
    const keyRemap = {
        _c: 'content',
        _t: 'type',
        _a: 'attrs',
        _d: 'dataId',
        _x: 'text'
    };

    const valueRemap = {
        _t: { p: 'paragraph', x: 'text' }
    };

    return baseEncode(encoded, { keyRemap, valueRemap });
};
