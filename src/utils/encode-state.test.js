import { encode, decode } from './encode-state';

test('it encode init state properly', () => {
    const state = { type: 'doc', content: [{ type: 'paragraph', attrs: { dataId: 'id' } }] };
    const expectedState = { _t: 'doc', _c: [{ _t: 'p', _a: { _d: 'id' } }] };
    expect(encode(state)).toEqual(expectedState);
});

test('it decode init state properly', () => {
    const state = { _t: 'doc', _c: [{ _t: 'p', _a: { _d: 'id' } }] };
    const expectedState = { type: 'doc', content: [{ type: 'paragraph', attrs: { dataId: 'id' } }] };
    expect(decode(state)).toEqual(expectedState);
});

test('it encode state with unknown attribute', () => {
    const state = { type: 'doc', unknown: 'unknown' };
    const expectedState = { _t: 'doc', unknown: 'unknown' };
    expect(encode(state)).toEqual(expectedState);
});

test('it decode state with unknown attribute', () => {
    const state = { _t: 'doc', unknown: 'unknown' };
    const expectedState = { type: 'doc', unknown: 'unknown' };
    expect(decode(state)).toEqual(expectedState);
});
